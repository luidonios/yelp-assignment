package com.quantrics.louiedonios.yelpassignment.broadcast

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build

object NetworkUtils {

    fun isOnline(context: Context): Boolean {
        val cm =
            context.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = cm.activeNetwork
            val networkCapabilities = cm.getNetworkCapabilities(network)
            networkCapabilities?.let {
                return it.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                        it.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
            }


        } else {
            cm.activeNetworkInfo?.let {
                return it.isConnected
            }
        }

        return false
    }
}