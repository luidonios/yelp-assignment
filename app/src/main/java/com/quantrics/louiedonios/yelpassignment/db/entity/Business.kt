package com.quantrics.louiedonios.yelpassignment.db.entity

import androidx.room.*

@Entity(tableName = "business")
data class Business(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "alias") val alias: String?,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "image_url") val imageUrl: String?,
    @ColumnInfo(name = "is_closed") val isClosed: Boolean?,
    @ColumnInfo(name = "url") val url: String?,
    @ColumnInfo(name = "review_count") val reviewCount: Int?,
    @ColumnInfo(name = "rating") val rating: Float?,
    @ColumnInfo(name = "coordinates") val coordinates: Coordinates?,
    @ColumnInfo(name = "price") val price: String?,
    @ColumnInfo(name = "phone") val phone: String?,
    @ColumnInfo(name = "display_phone") val displayPhone: String?,
    @ColumnInfo(name = "distance") val distance: Double?,
    @ColumnInfo(name = "categories") val categories: String?
)



