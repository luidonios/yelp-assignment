package com.quantrics.louiedonios.yelpassignment.db.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "category",
    foreignKeys = [ForeignKey(
        entity = Business::class,
        parentColumns = ["id"],
        childColumns = ["businessId"],
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index("businessId")]
)
data class Category(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,
    val alias: String?,
    val title: String?,
    val businessId: String
)