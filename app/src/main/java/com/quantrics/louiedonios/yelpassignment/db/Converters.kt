package com.quantrics.louiedonios.yelpassignment.db


import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.quantrics.louiedonios.yelpassignment.db.entity.Coordinates

class CoordinatesTypeConverter {

    @TypeConverter
    fun toJson(value: Coordinates): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun fromJson(value: String): Coordinates {
        return Gson().fromJson<Coordinates>(value, object: TypeToken<Coordinates>(){}.type)
    }
}

class StringListTypeConverter {

    @TypeConverter
    fun toJson(value: List<String>): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun fromJson(value: String): List<String> {
        return Gson().fromJson<List<String>>(value, object: TypeToken<List<String>>(){}.type)
    }
}