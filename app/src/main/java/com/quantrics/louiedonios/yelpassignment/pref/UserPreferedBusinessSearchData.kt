package com.quantrics.louiedonios.yelpassignment.pref

data class UserPreferedBusinessSearchData(
    val businessName: String,
    val address: String,
    val city:String,
    val postalCode: String,
    val category: String,
    val isAsc: Boolean
)