package com.quantrics.louiedonios.yelpassignment.repository

import android.content.Context
import com.quantrics.louiedonios.yelpassignment.api.*
import com.quantrics.louiedonios.yelpassignment.db.YelpAssignmentDatabase
import com.quantrics.louiedonios.yelpassignment.db.dao.BusinessDao
import com.quantrics.louiedonios.yelpassignment.db.dao.LocationDao
import com.quantrics.louiedonios.yelpassignment.db.entity.BusinessAndLocation
import com.quantrics.louiedonios.yelpassignment.pref.SharedPref
import com.quantrics.louiedonios.yelpassignment.pref.UserPreferedBusinessSearchData
import com.quantrics.louiedonios.yelpassignment.pref.YelpAssignmentSharedPref
import kotlinx.coroutines.CoroutineScope

class BusinessesRepository(
    private val businessDao: BusinessDao,
    private val locationDao: LocationDao,
    private val sharedPreferences: SharedPref,
    private val businessesService: BusinessesService
) : BusinessContract.Repository {

    override fun searchNearbyBusinesses(
        coroutineScope: CoroutineScope,
        coordinatesData: CoordinatesData,
        offset: Int,
        limit: Int
    ) = object :
        NetworkBoundResource<List<BusinessAndLocation>, SearchResponse>(coroutineScope) {

        override fun saveCallResult(item: SearchResponse) {

            item.total?.let {
                sharedPreferences.saveNearbyBusinessCount(it)
            }

            item.businesses?.let {
                saveBusinessWithLocationToDb(it)
            }
        }

        private fun saveBusinessWithLocationToDb(businesses: List<BusinessData>) {

            businessDao.insertAll(businesses.mapToLocalDataSource())

            businesses.forEach { business ->
                saveLocationWithBusinesIdToDb(business.location, business.id)

            }
        }

        private fun saveLocationWithBusinesIdToDb(location: LocationData?, businessId: String) {
            location?.let { locationData ->
                locationDao.insert(locationData.mapToLocalDataSource(businessId))
            }
        }

        override fun shouldFetch(data: List<BusinessAndLocation>?) =
            true


        override fun loadFromDb() =
            businessDao.getBusinesses(limit, offset)


        override fun createCall() =
            businessesService.searchBusinesses(
                coordinatesData.latitude!!,
                coordinatesData.longitude!!,
                offset,
                limit
            )


    }.asLiveData()

    override fun getNearbyBusinessesCount() =
        sharedPreferences.getNearbyBusinessCount()

    override fun searchBusinessesByUserPref() =
        businessDao.searchBusinessesByUserPref(
            name = "%${sharedPreferences.getPreferedBusinessName()}%",
            categories = "%${sharedPreferences.getPreferedBusinessCategory()}%",
            address = "%${sharedPreferences.getPreferedBusinessAddress()}%",
            city = "%${sharedPreferences.getPreferedCity()}%",
            postalCode = "%${sharedPreferences.getPreferedPostalCode()}%",
            isAsc = sharedPreferences.isOrderByDistanceAsc()
        )

    override fun deleteBusinesses() {
        businessDao.deleteBusinesses()
    }

    override fun saveUserPreferedBusinessDetails(
        businessName: String,
        businessCategory: String,
        businessAddress: String,
        city: String,
        postalCode: String,
        isAsc: Boolean
    ) {
        sharedPreferences.savePreferedBusinessName(businessName)
        sharedPreferences.savePreferedBusinessCategory(businessCategory)
        sharedPreferences.savePreferedBusinessAddress(businessAddress)
        sharedPreferences.savePreferedCity(city)
        sharedPreferences.savePreferedPostalCode(postalCode)
        sharedPreferences.saveIsOrderByDistanceAsc(isAsc)
    }

    override fun getUserPreferedBusinessSearch() = UserPreferedBusinessSearchData(
        sharedPreferences.getPreferedBusinessName(),
        sharedPreferences.getPreferedBusinessAddress(),
        sharedPreferences.getPreferedCity(),
        sharedPreferences.getPreferedPostalCode(),
        sharedPreferences.getPreferedBusinessCategory(),
        sharedPreferences.isOrderByDistanceAsc()
    )

    override fun saveLastKnownLocation(latitude: Double, longitude: Double) {
        sharedPreferences.saveLastKnownLatitude(latitude)
        sharedPreferences.saveLastKnownLongitude(longitude)
    }

    override fun getLastKnownLocation() =
        CoordinatesData(
            sharedPreferences.getLastKnownLatitude(),
            sharedPreferences.getLastKnownLongitude()
        )


    companion object {
        fun createInstance(context: Context): BusinessesRepository {

            val database = YelpAssignmentDatabase.getInstance(context)
            val businessDao = database!!.getBusinessDao()
            val locationDao = database.getLocationDao()
            val sharedPreferences = YelpAssignmentSharedPref(context)
            val businessesService = BusinessesService.create()
            return BusinessesRepository(
                businessDao,
                locationDao,
                sharedPreferences,
                businessesService
            )
        }
    }
}