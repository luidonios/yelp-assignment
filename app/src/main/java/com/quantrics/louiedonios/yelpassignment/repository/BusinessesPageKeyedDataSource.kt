package com.quantrics.louiedonios.yelpassignment.repository

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.paging.PageKeyedDataSource
import com.quantrics.louiedonios.yelpassignment.api.CoordinatesData
import com.quantrics.louiedonios.yelpassignment.api.Resource
import com.quantrics.louiedonios.yelpassignment.db.entity.BusinessAndLocation
import com.quantrics.louiedonios.yelpassignment.ui.businesses.BusinessesListAdapterData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BusinessesPageKeyedDataSource(
    private val coroutineScope: CoroutineScope,
    private val lifecycleOwner: LifecycleOwner,
    private val businessesRepository: BusinessesRepository
) : PageKeyedDataSource<Int, BusinessesListAdapterData>() {


    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, BusinessesListAdapterData>
    ) {
        coroutineScope.launch(Dispatchers.Main) {
            loadInitialListAndUpdateNextPageKey(callback)
        }
    }

    @MainThread
    private fun loadInitialListAndUpdateNextPageKey(
        callback: LoadInitialCallback<Int, BusinessesListAdapterData>
    ) {

        val searchBusinessesResultLiveData =
            businessesRepository.searchNearbyBusinesses(
                coroutineScope,
                businessesRepository.getLastKnownLocation(),
                INITIAL_OFFSET,
                LIMIT
            )

        //Observer should not be converted to lambda,
        //We need a reference to the observer to remove it later
        //after request is completed or failed
        searchBusinessesResultLiveData.observe(
            lifecycleOwner,
            object : Observer<Resource<List<BusinessAndLocation>>> {
                override fun onChanged(resource: Resource<List<BusinessAndLocation>>?) {
                    when (resource) {
                        is Resource.Loading -> Unit
                        is Resource.Success, is Resource.Error -> {

                            //Only remove the observer
                            //when there if data is not empty
                            if (resource.isDataNotEmpty()) {
                                processAndEmitResourceData(resource.data!!, callback)
                                searchBusinessesResultLiveData.removeObserver(this)
                            }

                        }
                    }
                }

            })
    }

    private fun Resource<List<BusinessAndLocation>>.isDataNotEmpty() =
        data?.isNotEmpty() ?: false

    private fun processAndEmitResourceData(
        data: List<BusinessAndLocation>,
        callback: LoadInitialCallback<Int, BusinessesListAdapterData>
    ) {

        callback.onResult(
            data.mapNearbySearchResultsToListAdapterData(),
            null,
            INITIAL_OFFSET + LIMIT
        )
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, BusinessesListAdapterData>
    ) {
        coroutineScope.launch(Dispatchers.Main) {
            loadPreviousListAndDecrementAdjacentPageKey(params.key, callback)
        }
    }

    @MainThread
    private fun loadPreviousListAndDecrementAdjacentPageKey(
        paramsKey: Int,
        callback: LoadCallback<Int, BusinessesListAdapterData>
    ) {
        val searchBusinessesResultLiveData =
            businessesRepository.searchNearbyBusinesses(
                coroutineScope,
                businessesRepository.getLastKnownLocation(),
                paramsKey,
                LIMIT
            )

        searchBusinessesResultLiveData.observe(lifecycleOwner,
            object : Observer<Resource<List<BusinessAndLocation>>> {
                override fun onChanged(resource: Resource<List<BusinessAndLocation>>?) {
                    when (resource) {
                        is Resource.Loading -> Unit
                        is Resource.Success, is Resource.Error -> {

                            if (resource.isDataNotEmpty()) {
                                processAndEmitResourceDataAndUpdatePrevKey(
                                    resource.data!!,
                                    callback,
                                    paramsKey
                                )
                                searchBusinessesResultLiveData.removeObserver(this)
                            }
                        }
                    }
                }

            })
    }

    private fun processAndEmitResourceDataAndUpdatePrevKey(
        data: List<BusinessAndLocation>,
        callback: LoadCallback<Int, BusinessesListAdapterData>,
        paramsKey: Int
    ) {
        val key = if (paramsKey > LIMIT) paramsKey - LIMIT else null
        callback.onResult(data.mapNearbySearchResultsToListAdapterData(), key)
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, BusinessesListAdapterData>
    ) {
        coroutineScope.launch(Dispatchers.Main) {
            loadNextListAndIncrementAdjacentPageKey(params.key, callback)
        }
    }

    @MainThread
    private fun loadNextListAndIncrementAdjacentPageKey(
        paramsKey: Int,
        callback: LoadCallback<Int, BusinessesListAdapterData>
    ) {

        val searchBusinessesResultLiveData =
            businessesRepository.searchNearbyBusinesses(
                coroutineScope,
                businessesRepository.getLastKnownLocation(),
                paramsKey,
                LIMIT
            )

        searchBusinessesResultLiveData.observe(
            lifecycleOwner,
            object : Observer<Resource<List<BusinessAndLocation>>> {
                override fun onChanged(resource: Resource<List<BusinessAndLocation>>?) {
                    when (resource) {
                        is Resource.Loading -> Unit
                        is Resource.Success, is Resource.Error -> {

                            if (resource.isDataNotEmpty()) {
                                processAndEmitResourceDataAndUpdateNextKey(
                                    resource.data!!,
                                    callback,
                                    paramsKey
                                )
                                searchBusinessesResultLiveData.removeObserver(this)
                            }

                        }
                    }
                }

            })
    }

    private fun processAndEmitResourceDataAndUpdateNextKey(
        data: List<BusinessAndLocation>,
        callback: LoadCallback<Int, BusinessesListAdapterData>,
        paramsKey: Int
    ) {
        val totalFetchedBusinesses = paramsKey + data.size
        val searchedResponseTotal =
            businessesRepository.getNearbyBusinessesCount()
        val hasMore = totalFetchedBusinesses < searchedResponseTotal
        val key = if (hasMore) paramsKey + LIMIT else null
        callback.onResult(data.mapNearbySearchResultsToListAdapterData(), key)
    }

    companion object {

        const val LIMIT = 10
        const val INITIAL_OFFSET = 0

        fun createInstance(
            coroutineScope: CoroutineScope,
            lifecycleOwner: LifecycleOwner,
            businessesRepository: BusinessesRepository
        ): BusinessesPageKeyedDataSource {
            return BusinessesPageKeyedDataSource(
                coroutineScope,
                lifecycleOwner,
                businessesRepository
            )
        }
    }
}