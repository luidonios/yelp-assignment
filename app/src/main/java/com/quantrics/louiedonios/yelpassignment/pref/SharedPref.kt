package com.quantrics.louiedonios.yelpassignment.pref

interface SharedPref {

    fun saveNearbyBusinessCount(count: Int)
    fun getNearbyBusinessCount(): Int

    fun savePreferedBusinessName(name: String)
    fun getPreferedBusinessName(): String

    fun savePreferedBusinessCategory(category: String)
    fun getPreferedBusinessCategory(): String

    fun savePreferedBusinessAddress(address: String)
    fun getPreferedBusinessAddress(): String

    fun savePreferedCity(city: String)
    fun getPreferedCity(): String

    fun savePreferedPostalCode(postalCode: String)
    fun getPreferedPostalCode():String

    fun saveIsOrderByDistanceAsc(isAsc: Boolean)
    fun isOrderByDistanceAsc(): Boolean

    fun saveLastKnownLatitude(latitude: Double)
    fun getLastKnownLatitude(): Double
    fun saveLastKnownLongitude(longitude: Double)
    fun getLastKnownLongitude():Double

    fun clear()

}