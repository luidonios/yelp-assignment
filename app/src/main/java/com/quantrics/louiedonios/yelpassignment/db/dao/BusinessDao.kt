package com.quantrics.louiedonios.yelpassignment.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.quantrics.louiedonios.yelpassignment.db.entity.Business
import com.quantrics.louiedonios.yelpassignment.db.entity.BusinessAndLocation
import com.quantrics.louiedonios.yelpassignment.db.entity.JoinedBusinessAndLocation

@Dao
interface BusinessDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(vararg business: Business)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(business: List<Business>)

    @Query("SELECT * FROM business LIMIT :limit OFFSET :offset")
    @Transaction
    fun getBusinesses(limit: Int, offset: Int): LiveData<List<BusinessAndLocation>>


    @Query(
        "SELECT * FROM business " +
                "INNER JOIN location ON location.businessId = business.id " +
                "WHERE business.name LIKE :name " +
                "AND business.categories LIKE :categories " +
                "AND (location.address1 LIKE :address " +
                "OR location.address2 LIKE :address " +
                "OR location.address3 LIKE :address) " +
                "AND location.city LIKE :city " +
                "AND location.zip_code LIKE :postalCode " +
                "ORDER BY " +
                "CASE WHEN :isAsc = 1 THEN business.distance END ASC, " +
                "CASE WHEN :isAsc = 0 THEN business.distance END DESC"
    )
    @Transaction
    fun searchBusinessesByUserPref(
        name: String = "",
        categories: String = "",
        address: String = "",
        city: String = "",
        postalCode: String = "",
        isAsc: Boolean = true
    ): LiveData<List<JoinedBusinessAndLocation>>

    @Query("DELETE FROM business")
    fun deleteBusinesses()

}