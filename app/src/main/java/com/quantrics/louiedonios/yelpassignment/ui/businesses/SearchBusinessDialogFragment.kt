package com.quantrics.louiedonios.yelpassignment.ui.businesses

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.quantrics.louiedonios.yelpassignment.R
import kotlinx.android.synthetic.main.fragment_search_business.view.*
import java.lang.ClassCastException


class SearchBusinessDialogFragment : DialogFragment() {

    private var onClickBtnApplyListener: OnClickBtnApplyListener? = null
    private var searchBusinessDialogData: SearchBusinessDialogData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val argsJson = it.getString(ARGS_DATA, "")
            if (argsJson.isNotEmpty()) {
                searchBusinessDialogData = Gson().fromJson(
                    argsJson,
                    object : TypeToken<SearchBusinessDialogData>() {}.type
                )
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            onClickBtnApplyListener = context as OnClickBtnApplyListener
        } catch (exception: ClassCastException) {
            throw ClassCastException("$context must implement OnClickBtnApplyListener!")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val view =
            LayoutInflater.from(requireContext()).inflate(R.layout.fragment_search_business, null)

        prefillForm(view)

        val builder = AlertDialog.Builder(requireContext())
            .setTitle("Search")
            .setView(view)
            .setPositiveButton("Apply") { _, _ ->
                with(view) {
                    onClickBtnApplyListener?.onClickBtnApply(
                        SearchBusinessDialogData(
                            businessName = et_business_name.text.toString(),
                            address = et_address.text.toString(),
                            city = et_city.text.toString(),
                            category = et_category.text.toString(),
                            postalCode = et_postal_code.text.toString(),
                            isDistanceOrderByAsc = view.cb_order_by_distance.isChecked
                        )
                    )
                }

            }
            .setNegativeButton("Cancel") { _, _ -> }

        return builder.create()
    }

    private fun prefillForm(view: View) {
        view.cb_order_by_distance.isChecked = true
        searchBusinessDialogData?.let {
            view.et_business_name.setText(it.businessName, TextView.BufferType.EDITABLE)
            view.et_address.setText(it.address, TextView.BufferType.EDITABLE)
            view.et_city.setText(it.city, TextView.BufferType.EDITABLE)
            view.et_postal_code.setText(it.postalCode, TextView.BufferType.EDITABLE)
            view.et_category.setText(it.category, TextView.BufferType.EDITABLE)
            view.cb_order_by_distance.isChecked = it.isDistanceOrderByAsc
        }
    }

    interface OnClickBtnApplyListener {
        fun onClickBtnApply(searchBusinessDialogData: SearchBusinessDialogData)
    }

    companion object {
        const val TAG = "SearchBusinessDialogFragment"

        private const val ARGS_DATA = "args_data"

        fun createInstance(
            searchBusinessDialogData: SearchBusinessDialogData?
        ): SearchBusinessDialogFragment {

            val searchBusinessDialogFragment = SearchBusinessDialogFragment()

            searchBusinessDialogData?.let {
                searchBusinessDialogFragment.arguments = Bundle().apply {
                    putString(ARGS_DATA, Gson().toJson(it))
                }
            }

            return searchBusinessDialogFragment
        }
    }
}

data class SearchBusinessDialogData(
    val businessName: String,
    val address: String,
    val city: String,
    val category: String,
    val postalCode: String,
    val isDistanceOrderByAsc: Boolean
)
