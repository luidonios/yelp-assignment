package com.quantrics.louiedonios.yelpassignment.api

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    val businesses: List<BusinessData>?,
    val total: Int?,
    val region: RegionData?
)

data class BusinessData(
    @SerializedName("id") val id: String,
    @SerializedName("alias") val alias: String?,
    @SerializedName("name") val name: String?,
    @SerializedName("image_url") val imageUrl: String?,
    @SerializedName("is_closed") val isClosed: Boolean?,
    @SerializedName("url") val url: String?,
    @SerializedName("review_count") val reviewCount: Int?,
    @SerializedName("categories") val categories: List<CategoryData>?,
    @SerializedName("rating") val rating: Float?,
    @SerializedName("coordinates") val coordinates: CoordinatesData?,
    @SerializedName("price") val price: String?,
    @SerializedName("location") val location: LocationData?,
    @SerializedName("phone") val phone: String?,
    @SerializedName("display_phone") val displayPhone: String?,
    @SerializedName("distance") val distance:Double?
    )

data class CategoryData(
    val alias: String?,
    val title: String?
)

data class CoordinatesData(
    val latitude: Double?,
    val longitude: Double?
)

data class LocationData(
    @SerializedName("address1") val address1: String?,
    @SerializedName("address2") val address2: String?,
    @SerializedName("address3") val address3: String?,
    @SerializedName("city") val city: String?,
    @SerializedName("zip_code") val zipCode: String?,
    @SerializedName("country") val country: String?,
    @SerializedName("state") val state: String?,
    @SerializedName("display_address") val displayAddress: List<String>?,
)

data class RegionData(
    val center: CoordinatesData?
)


