package com.quantrics.louiedonios.yelpassignment.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class NetworkChangeReceiver: BroadcastReceiver() {

    private var onNetworkChangeListener:OnNetworkChangeListener? = null

    fun setOnNetworkChangeListener(onNetworkChangeListener:OnNetworkChangeListener) {
        this.onNetworkChangeListener = onNetworkChangeListener
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        context?.let {
            val isOnline = NetworkUtils.isOnline(it)
            onNetworkChangeListener?.onNetworkChange(isOnline)
        }
    }

    interface OnNetworkChangeListener {
        fun onNetworkChange(isOnline:Boolean)
    }
}