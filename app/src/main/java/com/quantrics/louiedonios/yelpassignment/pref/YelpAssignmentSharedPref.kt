package com.quantrics.louiedonios.yelpassignment.pref

import android.content.Context

class YelpAssignmentSharedPref(context: Context): SharedPref {

    private val sharedPreferences =
        context.applicationContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

    override fun saveNearbyBusinessCount(count: Int) {
        sharedPreferences.edit().putInt(PREF_NEARBY_BUSINESS_COUNT, count).apply()
    }

    override fun getNearbyBusinessCount() =
        sharedPreferences.getInt(PREF_NEARBY_BUSINESS_COUNT, 0)

    override fun savePreferedBusinessName(name: String) {
        sharedPreferences.edit().putString(PREF_BUSINESS_NAME, name).apply()
    }

    override fun getPreferedBusinessName() =
        sharedPreferences.getString(PREF_BUSINESS_NAME, "") ?: ""


    override fun savePreferedBusinessCategory(category: String) {
        sharedPreferences.edit().putString(PREF_BUSINESS_CATEGORY, category).apply()
    }

    override fun getPreferedBusinessCategory() =
        sharedPreferences.getString(PREF_BUSINESS_CATEGORY, "") ?: ""


    override fun savePreferedBusinessAddress(address: String) {
        sharedPreferences.edit().putString(PREF_BUSINESS_ADDRESS, address).apply()
    }

    override fun getPreferedBusinessAddress() =
        sharedPreferences.getString(PREF_BUSINESS_ADDRESS, "") ?: ""

    override fun savePreferedCity(city: String) {
        sharedPreferences.edit().putString(PREF_CITY, city).apply()
    }

    override fun getPreferedCity() =
        sharedPreferences.getString(PREF_CITY, "") ?: ""

    override fun savePreferedPostalCode(postalCode: String) {
        sharedPreferences.edit().putString(PREF_POSTAL_CODE, postalCode).apply()
    }

    override fun getPreferedPostalCode() =
        sharedPreferences.getString(PREF_POSTAL_CODE, "") ?: ""


    override fun saveIsOrderByDistanceAsc(isAsc: Boolean) {
        sharedPreferences.edit().putBoolean(PREF_IS_ASC, isAsc).apply()
    }

    override fun isOrderByDistanceAsc() =
        sharedPreferences.getBoolean(PREF_IS_ASC, true)

    override fun saveLastKnownLatitude(latitude: Double) {
        sharedPreferences.edit().putString(PREF_LATITUDE, "$latitude").apply()
    }

    override fun getLastKnownLatitude() =
        (sharedPreferences.getString(PREF_LATITUDE, "0.0") ?: "").toDouble()


    override fun saveLastKnownLongitude(longitude: Double) {
        sharedPreferences.edit().putString(PREF_LONGITUDE, "$longitude").apply()
    }

    override fun getLastKnownLongitude() =
        (sharedPreferences.getString(PREF_LONGITUDE, "0.0") ?: "").toDouble()

    override fun clear() {
        sharedPreferences.edit().clear().apply()
    }

    companion object {
        private const val SHARED_PREF_NAME = "yelp_assignment_shared_pref"
        private const val PREF_NEARBY_BUSINESS_COUNT = "pref_nearby_business_count"
        private const val PREF_BUSINESS_NAME = "pref_business_name"
        private const val PREF_BUSINESS_ADDRESS = "pref_business_address"
        private const val PREF_CITY = "pref_city"
        private const val PREF_POSTAL_CODE = "pref_postal_code"
        private const val PREF_BUSINESS_CATEGORY = "pref_business_category"
        private const val PREF_IS_ASC = "pref_is_asc"
        private const val PREF_LATITUDE = "pref_latitude"
        private const val PREF_LONGITUDE = "pref_longitude"
    }
}