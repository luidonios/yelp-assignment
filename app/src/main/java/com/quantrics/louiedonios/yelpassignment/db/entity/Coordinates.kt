package com.quantrics.louiedonios.yelpassignment.db.entity

data class Coordinates(val latitude: Double?, val longitude: Double?)