package com.quantrics.louiedonios.yelpassignment.repository

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.quantrics.louiedonios.yelpassignment.ui.businesses.BusinessesListAdapterData
import kotlinx.coroutines.CoroutineScope
import java.lang.IllegalArgumentException

class BusinessesDataSourceFactory(
    private val owner: LifecycleOwner,
    private val businessesRepository: BusinessesRepository
) : DataSource.Factory<Int, BusinessesListAdapterData>() {

    private var coroutineScope: CoroutineScope? = null
    private var pageKeyedDataSource: PageKeyedDataSource<Int, BusinessesListAdapterData>? = null

    private var _dataSourceLiveData: MutableLiveData<PageKeyedDataSource<Int, BusinessesListAdapterData>> =
        MutableLiveData()
    val dataSourceLiveData: LiveData<PageKeyedDataSource<Int, BusinessesListAdapterData>> =
        _dataSourceLiveData

    fun setCoroutineScope(coroutineScope: CoroutineScope) {
        this.coroutineScope = coroutineScope
    }

    fun create(pageListDataSource: PageListDataSource) {
        when (pageListDataSource) {
            PageListDataSource.UserPrefBusinesses -> {
                pageKeyedDataSource =
                    SearchBusinessesPageKeyedDataSource.createInstance(
                        coroutineScope!!,
                        owner,
                        businessesRepository
                    )
            }
            else -> {
                pageKeyedDataSource =
                    BusinessesPageKeyedDataSource.createInstance(
                        coroutineScope!!,
                        owner,
                        businessesRepository
                    )
            }
        }
    }

    override fun create(): DataSource<Int, BusinessesListAdapterData> {
        _dataSourceLiveData.postValue(pageKeyedDataSource)
        return pageKeyedDataSource!!


    }


}

enum class PageListDataSource {
    NearbyBusinesses,
    UserPrefBusinesses
}