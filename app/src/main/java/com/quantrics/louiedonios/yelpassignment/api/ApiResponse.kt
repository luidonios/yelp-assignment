package com.quantrics.louiedonios.yelpassignment.api

import retrofit2.Response

class ApiResponse<T> {

    var isSuccessful: Boolean = false
    var body:T? = null
    var errorMessage:String? = null

    constructor(error: Throwable) {
        isSuccessful = false
        body = null
        errorMessage = error.message
    }

    constructor(response: Response<T>) {
        isSuccessful = true
        body = response.body()
        errorMessage = null
    }
}