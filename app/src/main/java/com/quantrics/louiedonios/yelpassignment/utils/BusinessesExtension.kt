package com.quantrics.louiedonios.yelpassignment.repository

import android.util.Log
import com.quantrics.louiedonios.yelpassignment.api.BusinessData
import com.quantrics.louiedonios.yelpassignment.api.LocationData
import com.quantrics.louiedonios.yelpassignment.db.entity.*
import com.quantrics.louiedonios.yelpassignment.ui.businesses.BusinessesListAdapterData

fun List<BusinessAndLocation>.mapNearbySearchResultsToListAdapterData(): List<BusinessesListAdapterData> {
    return map { businessAndLocation ->

        var businessesListAdapterData: BusinessesListAdapterData

        with(businessAndLocation) {

            val displayAddress =
                location.displayAddress?.joinToString("\n") ?: ""

            with(business) {
                businessesListAdapterData = BusinessesListAdapterData(
                    id = id,
                    name = name ?: "",
                    imageUrl = imageUrl ?: "/",
                    categories = categories ?: "",
                    hoursOfOperation = "",
                    address = displayAddress,
                    rating = "$rating",
                    phone = displayPhone ?: ""
                )
            }
        }

        businessesListAdapterData
    }
}

fun List<JoinedBusinessAndLocation>.mapUserPreferedSearchResultsToListAdapterData(): List<BusinessesListAdapterData> {
    return map { businessAndLocation ->

        var businessesListAdapterData: BusinessesListAdapterData

        with(businessAndLocation) {

            val displayAddress =
                displayAddress?.joinToString("\n") ?: ""
            businessesListAdapterData = BusinessesListAdapterData(
                id = id,
                name = name ?: "",
                imageUrl = imageUrl ?: "/",
                categories = categories ?: "",
                hoursOfOperation = "",
                address = displayAddress,
                rating = "$rating",
                phone = displayPhone ?: ""
            )

        }

        businessesListAdapterData
    }
}

fun LocationData.mapToLocalDataSource(businessId: String) =
    Location(
        address1 = address1,
        address2 = address2,
        address3 = address3,
        city = city,
        zipCode = zipCode,
        country = country,
        state = state,
        displayAddress = displayAddress,
        businessId = businessId
    )

fun List<BusinessData>.mapToLocalDataSource() =
    map { data ->
        Business(
            id = data.id,
            alias = data.alias,
            name = data.name,
            imageUrl = data.imageUrl,
            isClosed = data.isClosed,
            url = data.url,
            reviewCount = data.reviewCount,
            rating = data.rating,
            coordinates = Coordinates(
                latitude = data.coordinates?.latitude,
                longitude = data.coordinates?.longitude
            ),
            price = data.price,
            phone = data.phone,
            displayPhone = data.displayPhone,
            distance = data.distance,
            categories = data.categories?.map { category -> category.title }
                ?.joinToString(", ")
        )
    }