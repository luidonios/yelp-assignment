package com.quantrics.louiedonios.yelpassignment.ui.businesses

import android.content.IntentFilter
import android.location.Location
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.quantrics.louiedonios.yelpassignment.R
import com.quantrics.louiedonios.yelpassignment.broadcast.NetworkChangeReceiver
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_location_permission_request.*


class MainActivity : LocationAwareActivity(),
    SearchBusinessDialogFragment.OnClickBtnApplyListener,
    NetworkChangeReceiver.OnNetworkChangeListener {

    private lateinit var businessesViewModel: BusinessesViewModel
    private lateinit var businessesListAdapter: BusinessesListAdapter

    private var networkChangeReceiver: NetworkChangeReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupBusinessesList()
        setupBusinessViewModel()
        setupBusinessSearchDialogObserver()
        setupSwipeToRefreshGesture()

    }

    private fun setupBusinessViewModel() {
        val businessesViewModelFactory = BusinessesViewModelFactory(this, this)
        businessesViewModel =
            ViewModelProvider(this, businessesViewModelFactory)
                .get(BusinessesViewModel::class.java)
    }

    override fun onReceiveLastLocation(location: Location) {

        businessesViewModel.saveLastLocationAndSearchNearbyBusiness(
            location.latitude, location.longitude
        )

        setupBusinessesListObserver()
        hideLocationPermissionRequest()

    }

    private fun hideLocationPermissionRequest() {
        if(location_permission_request.visibility == View.VISIBLE){
            location_permission_request.visibility = View.GONE
        }
    }

    override fun onUserDeniedPermission() {
        location_permission_request.visibility = View.VISIBLE
        btn_allow.setOnClickListener {
            requestAndGetLastLocation()
        }
    }

    private fun setupBusinessSearchDialogObserver() {
        businessesViewModel.searchBusinessDialogDataLiveData.observe(this, {
            showSearchBusinessesDialogWithData(it)
        })
    }

    private fun showSearchBusinessesDialogWithData(data: SearchBusinessDialogData?) {
        val searchBusinessDialogFragment = SearchBusinessDialogFragment.createInstance(data)
        searchBusinessDialogFragment.show(
            supportFragmentManager,
            SearchBusinessDialogFragment.TAG
        )
    }

    private fun setupSwipeToRefreshGesture() {
        swiperefresh.setOnRefreshListener {
            businessesViewModel.searchNearbyBusinesses()
        }
    }

    private fun setupBusinessesListObserver() {
        businessesViewModel.businessesPageListLiveData?.observe(this, {
            if (swiperefresh.isRefreshing) swiperefresh.isRefreshing = false
            businessesListAdapter.submitList(it)
        })
    }

    private fun setupBusinessesList() {
        val linearLayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_businesses.layoutManager = linearLayoutManager

        val decor = DividerItemDecoration(this, linearLayoutManager.orientation)
        rv_businesses.addItemDecoration(decor)
        businessesListAdapter = BusinessesListAdapter()
        rv_businesses.adapter = businessesListAdapter
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(networkChangeReceiver)
        networkChangeReceiver = null
    }

    override fun onResume() {
        super.onResume()

        networkChangeReceiver = NetworkChangeReceiver()

        networkChangeReceiver?.setOnNetworkChangeListener(this)

        registerReceiver(networkChangeReceiver, IntentFilter().apply {
            addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> {
                businessesViewModel.showSearchBusinessesDialogAndSetPrevInputs()
            }
            else -> super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onClickBtnApply(searchBusinessDialogData: SearchBusinessDialogData) {
        businessesViewModel.saveUserPreferedBusinessDetails(searchBusinessDialogData)
        businessesViewModel.searchBusinessesByUserPref()
    }

    override fun onNetworkChange(isOnline: Boolean) {
        tv_no_internet.visibility = if (isOnline) View.GONE else View.VISIBLE
    }
}