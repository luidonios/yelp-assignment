package com.quantrics.louiedonios.yelpassignment.repository

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.paging.PageKeyedDataSource
import com.quantrics.louiedonios.yelpassignment.db.entity.JoinedBusinessAndLocation
import com.quantrics.louiedonios.yelpassignment.ui.businesses.BusinessesListAdapterData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchBusinessesPageKeyedDataSource(
    private val coroutineScope: CoroutineScope,
    private val lifecycleOwner: LifecycleOwner,
    private val businessesRepository: BusinessesRepository
) :
    PageKeyedDataSource<Int, BusinessesListAdapterData>() {

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, BusinessesListAdapterData>
    ) {

        val searchResultLiveData =
            businessesRepository.searchBusinessesByUserPref()

        coroutineScope.launch(Dispatchers.Main) {
            searchResultLiveData.observe(
                lifecycleOwner,
                object : Observer<List<JoinedBusinessAndLocation>> {
                    override fun onChanged(data: List<JoinedBusinessAndLocation>?) {
                        data?.let {
                            callback.onResult(data.mapUserPreferedSearchResultsToListAdapterData(), null, null)

                        }
                        searchResultLiveData.removeObserver(this)
                    }

                })
        }

    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, BusinessesListAdapterData>
    ) = Unit

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, BusinessesListAdapterData>
    ) = Unit

    companion object {
        fun createInstance(
            coroutineScope: CoroutineScope,
            lifecycleOwner: LifecycleOwner,
            businessesRepository: BusinessesRepository
        ): SearchBusinessesPageKeyedDataSource {

            return SearchBusinessesPageKeyedDataSource(
                coroutineScope,
                lifecycleOwner, businessesRepository
            )
        }
    }
}