package com.quantrics.louiedonios.yelpassignment.ui.businesses

data class BusinessesListAdapterData(
    val id: String,
    val name: String,
    val imageUrl: String,
    val categories: String,
    val hoursOfOperation: String,
    val address: String,
    val rating: String,
    val phone: String
) {
    override fun equals(other: Any?) =
        other is BusinessesListAdapterData &&
                id == other.id &&
                name == other.name &&
                imageUrl == other.imageUrl &&
                categories == other.categories &&
                hoursOfOperation == other.hoursOfOperation &&
                address == other.address &&
                rating == other.rating &&
                phone == other.phone
}