package com.quantrics.louiedonios.yelpassignment.db.entity

import androidx.room.*

@Entity(
    tableName = "location",
    foreignKeys = [ForeignKey(
        entity = Business::class,
        parentColumns = ["id"],
        childColumns = ["businessId"],
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index("businessId", unique = true)]
)
data class Location(
    @PrimaryKey(autoGenerate = true) val id: Long = 0L,
    @ColumnInfo(name = "address1") val address1: String?,
    @ColumnInfo(name = "address2") val address2: String?,
    @ColumnInfo(name = "address3") val address3: String?,
    @ColumnInfo(name = "city") val city: String?,
    @ColumnInfo(name = "zip_code") val zipCode: String?,
    @ColumnInfo(name = "country") val country: String?,
    @ColumnInfo(name = "state") val state: String?,
    @ColumnInfo(name = "display_address") val displayAddress: List<String>?,
    @ColumnInfo(name = "businessId") val businessId: String
)