package com.quantrics.louiedonios.yelpassignment.ui.businesses

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.quantrics.louiedonios.yelpassignment.R
import kotlinx.android.synthetic.main.view_holder_businesses.view.*

class BusinessesListAdapter :
    PagedListAdapter<BusinessesListAdapterData, BusinessesListAdapter.ViewHolder>(DIFF_UTIL) {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val ivThumbnail = itemView.iv_thumbnail
        private val tvName: TextView = itemView.tv_name
        private val tvCategories: TextView = itemView.tv_categories
        private val tvRating: TextView =  itemView.tv_rating
        private val tvAddress: TextView = itemView.tv_address
        private val tvPhone: TextView = itemView.tv_phone

        fun bindData(data: BusinessesListAdapterData) {
            with(data){

                Glide.with(itemView.context)
                    .load(imageUrl)
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(ivThumbnail)
                
                tvName.text = name
                tvCategories.text = categories
                tvRating.text = "Rating: $rating"
                tvAddress.text = "Address:\n$address"
                tvPhone.text = "Contact information:\n$phone"

                tvPhone.visibility = if(phone.isNotEmpty()) View.VISIBLE else View.GONE
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.view_holder_businesses, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val data: BusinessesListAdapterData? = getItem(position)
        data?.let { holder.bindData(it) }

    }

    companion object {

        private val DIFF_UTIL = object : DiffUtil.ItemCallback<BusinessesListAdapterData>() {
            override fun areItemsTheSame(
                oldItem: BusinessesListAdapterData,
                newItem: BusinessesListAdapterData
            ) = oldItem.id == newItem.id


            override fun areContentsTheSame(
                oldItem: BusinessesListAdapterData,
                newItem: BusinessesListAdapterData
            ) = oldItem == newItem

        }
    }
}