package com.quantrics.louiedonios.yelpassignment.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.quantrics.louiedonios.yelpassignment.db.dao.BusinessDao
import com.quantrics.louiedonios.yelpassignment.db.dao.LocationDao
import com.quantrics.louiedonios.yelpassignment.db.entity.Business
import com.quantrics.louiedonios.yelpassignment.db.entity.Location

@Database(entities = [Business::class, Location::class], version = 1)
@TypeConverters(CoordinatesTypeConverter::class, StringListTypeConverter::class)
abstract class YelpAssignmentDatabase: RoomDatabase() {

    abstract fun getBusinessDao(): BusinessDao
    abstract fun getLocationDao(): LocationDao

    companion object {

        private var INSTANCE: YelpAssignmentDatabase? = null
        fun getInstance(context: Context): YelpAssignmentDatabase? {

            if (INSTANCE == null){
                synchronized(YelpAssignmentDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        YelpAssignmentDatabase::class.java,
                        "yelp-assignment-db").build()
                }
            }
            return INSTANCE
        }
    }
}