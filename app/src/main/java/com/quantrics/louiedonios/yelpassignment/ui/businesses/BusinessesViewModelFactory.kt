package com.quantrics.louiedonios.yelpassignment.ui.businesses

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.quantrics.louiedonios.yelpassignment.repository.BusinessesDataSourceFactory
import com.quantrics.louiedonios.yelpassignment.repository.BusinessesRepository

class BusinessesViewModelFactory(
    private val owner: LifecycleOwner,
    private val context: Context
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        val businessRepository = BusinessesRepository.createInstance(context)
        val businessesDataSourceFactory = BusinessesDataSourceFactory(owner, businessRepository)

        return BusinessesViewModel(businessRepository, businessesDataSourceFactory) as T
    }
}