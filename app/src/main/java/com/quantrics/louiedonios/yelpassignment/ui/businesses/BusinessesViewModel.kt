package com.quantrics.louiedonios.yelpassignment.ui.businesses

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.quantrics.louiedonios.yelpassignment.repository.BusinessesDataSourceFactory
import com.quantrics.louiedonios.yelpassignment.repository.BusinessesPageKeyedDataSource
import com.quantrics.louiedonios.yelpassignment.repository.BusinessesRepository
import com.quantrics.louiedonios.yelpassignment.repository.PageListDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BusinessesViewModel(
    private val businessesRepository: BusinessesRepository,
    private val businessesDataSourceFactory: BusinessesDataSourceFactory
) : ViewModel() {

    var businessesPageListLiveData: LiveData<PagedList<BusinessesListAdapterData>>? = null

    private var _searchBusinessDialogDataLiveData: MutableLiveData<SearchBusinessDialogData> =
        MutableLiveData()
    val searchBusinessDialogDataLiveData: LiveData<SearchBusinessDialogData> =
        _searchBusinessDialogDataLiveData

    fun searchNearbyBusinesses() {

        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(BusinessesPageKeyedDataSource.LIMIT)
            .build()

        businessesDataSourceFactory.setCoroutineScope(viewModelScope)
        businessesDataSourceFactory.dataSourceLiveData.value?.invalidate()
        businessesDataSourceFactory.create(PageListDataSource.NearbyBusinesses)
        businessesPageListLiveData =
            LivePagedListBuilder(businessesDataSourceFactory, config).build()

    }

    fun saveUserPreferedBusinessDetails(searchBusinessDialogData: SearchBusinessDialogData) {
        with(searchBusinessDialogData) {
            businessesRepository.saveUserPreferedBusinessDetails(
                businessName,
                category,
                address,
                city,
                postalCode,
                isDistanceOrderByAsc
            )
        }
    }

    fun searchBusinessesByUserPref() {
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .build()

        businessesDataSourceFactory.setCoroutineScope(viewModelScope)
        businessesDataSourceFactory.dataSourceLiveData.value?.invalidate()
        businessesDataSourceFactory.create(PageListDataSource.UserPrefBusinesses)
        businessesPageListLiveData =
            LivePagedListBuilder(businessesDataSourceFactory, config).build()
    }

    fun showSearchBusinessesDialogAndSetPrevInputs() =
        viewModelScope.launch(Dispatchers.IO) {

            val userPreferedBusinessSearchData =
                businessesRepository.getUserPreferedBusinessSearch()

            with(userPreferedBusinessSearchData) {
                _searchBusinessDialogDataLiveData.postValue(
                    SearchBusinessDialogData(
                        businessName = businessName,
                        address = address,
                        city = city,
                        postalCode = postalCode,
                        category = category,
                        isDistanceOrderByAsc = isAsc
                    )
                )
            }
        }

    fun saveLastLocationAndSearchNearbyBusiness(latitude: Double, longitude: Double) {
        clearPrevRecordsIfLocationIsUpdated(latitude, longitude)
        businessesRepository.saveLastKnownLocation(latitude, longitude)
        searchNearbyBusinesses()
    }

    private fun clearPrevRecordsIfLocationIsUpdated(latitude: Double, longitude: Double) {
        val prevLastLocation = businessesRepository.getLastKnownLocation()
        if (prevLastLocation.latitude != latitude || prevLastLocation.longitude != longitude) {

        }


    }


}