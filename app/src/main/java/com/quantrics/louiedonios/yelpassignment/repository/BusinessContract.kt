package com.quantrics.louiedonios.yelpassignment.repository

import androidx.lifecycle.LiveData
import com.quantrics.louiedonios.yelpassignment.api.CoordinatesData
import com.quantrics.louiedonios.yelpassignment.api.LocationData
import com.quantrics.louiedonios.yelpassignment.api.Resource
import com.quantrics.louiedonios.yelpassignment.db.entity.BusinessAndLocation
import com.quantrics.louiedonios.yelpassignment.db.entity.JoinedBusinessAndLocation
import com.quantrics.louiedonios.yelpassignment.pref.UserPreferedBusinessSearchData
import kotlinx.coroutines.CoroutineScope

interface BusinessContract {

    interface Repository {
        fun searchNearbyBusinesses(
            coroutineScope: CoroutineScope,
            coordinatesData: CoordinatesData,
            offset: Int,
            limit: Int
        ): LiveData<Resource<List<BusinessAndLocation>>>

        fun getNearbyBusinessesCount(): Int

        fun searchBusinessesByUserPref(): LiveData<List<JoinedBusinessAndLocation>>

        fun deleteBusinesses()

        fun saveUserPreferedBusinessDetails(
            businessName: String,
            businessCategory: String,
            businessAddress: String,
            city:String,
            postalCode: String,
            isAsc: Boolean
        )

        fun getUserPreferedBusinessSearch(): UserPreferedBusinessSearchData

        fun saveLastKnownLocation(latitude: Double, longitude: Double)
        fun getLastKnownLocation(): CoordinatesData
    }
}