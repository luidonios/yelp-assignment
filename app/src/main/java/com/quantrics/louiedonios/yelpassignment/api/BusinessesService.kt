package com.quantrics.louiedonios.yelpassignment.api

import android.util.Log
import androidx.lifecycle.LiveData
import com.quantrics.louiedonios.yelpassignment.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface BusinessesService {

    @GET("businesses/search")
    fun searchBusinesses(
        @Query("latitude") latitude: Double,
        @Query("longitude") longitude: Double,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): LiveData<ApiResponse<SearchResponse>>

    companion object {

        fun create(): BusinessesService {

            val logger =
                HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { Log.d("API", "$it") })
            logger.level = HttpLoggingInterceptor.Level.BODY

            val authentication = object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {
                    val request = chain.request()
                    val authenticatedRequest =
                        request.newBuilder().addHeader(
                            "Authorization",
                            "Bearer ${BuildConfig.YELP_API_KEY}"
                        ).build()

                    return chain.proceed(authenticatedRequest)
                }

            }

            val okhttpClient = OkHttpClient.Builder()
                .addInterceptor(logger)
                .addInterceptor(authentication)
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okhttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .build()

            return retrofit.create(BusinessesService::class.java)
        }
    }
}


