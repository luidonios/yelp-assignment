package com.quantrics.louiedonios.yelpassignment.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.quantrics.louiedonios.yelpassignment.db.entity.Location

@Dao
interface LocationDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(vararg location: Location)

}