package com.quantrics.louiedonios.yelpassignment.db.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.PrimaryKey

data class JoinedBusinessAndLocation(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "image_url") val imageUrl: String?,
    @ColumnInfo(name = "rating") val rating: Float?,
    @ColumnInfo(name = "display_phone") val displayPhone: String?,
    @ColumnInfo(name = "distance") val distance: Double?,
    @ColumnInfo(name = "categories") val categories: String?,

    @ColumnInfo(name = "address1") val address1: String?,
    @ColumnInfo(name = "address2") val address2: String?,
    @ColumnInfo(name = "address3") val address3: String?,
    @ColumnInfo(name = "city") val city: String?,
    @ColumnInfo(name = "zip_code") val zipCode: String?,
    @ColumnInfo(name = "country") val country: String?,
    @ColumnInfo(name = "state") val state: String?,
    @ColumnInfo(name = "display_address") val displayAddress: List<String>?
)