package com.quantrics.louiedonios.yelpassignment.db.entity

import androidx.room.Embedded
import androidx.room.Relation

data class BusinessAndLocation(
    @Embedded
    val business: Business,

    @Relation(parentColumn = "id", entityColumn = "businessId")
    val location: Location
)