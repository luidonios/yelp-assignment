package com.quantrics.louiedonios.yelpassignment.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.quantrics.louiedonios.yelpassignment.api.ApiResponse
import com.quantrics.louiedonios.yelpassignment.api.Resource
import kotlinx.coroutines.*

abstract class NetworkBoundResource<ResultType, RequestType>(
    private val coroutineScope: CoroutineScope
) {

    private var result: MediatorLiveData<Resource<ResultType>> = MediatorLiveData()

    init {

        coroutineScope.launch(Dispatchers.Main) {
            //set Loading as initial value for result
            result.value = Resource.Loading()

            val dbSource = loadFromDb()
            result.addSource(dbSource) { data ->
                result.removeSource(dbSource)

                //check if we should fetch from network
                if (shouldFetch(data)) {
                    fetchFromNetwork(dbSource)
                } else {
                    //Emit data from database
                    result.addSource(dbSource) { data ->
                        setValue(Resource.Success(data))
                    }
                }
            }
        }


    }

    private fun fetchFromNetwork(dbSource: LiveData<ResultType>) {
        //emit Loading status
        //with data from database
        result.addSource(dbSource) { data ->
            result.value = Resource.Loading(data)
        }

        val apiSource = createCall()

        result.addSource(apiSource) { response ->
            result.removeSource(dbSource)
            result.removeSource(apiSource)

            if (response.isSuccessful) {
                coroutineScope.launch(Dispatchers.IO) {
                    response.body?.let {
                        saveCallResult(it)
                    }

                    withContext(Dispatchers.Main) {
                        //Emit Success status with data from updated database
                        result.addSource(dbSource) { data ->
                            setValue(Resource.Success(data))
                        }
                    }
                }
            } else {
                onFetchFailed()
                //Emit Error status with old data from database
                result.addSource(dbSource) { data ->
                    val errorMessage = response.errorMessage ?: ""
                    setValue(Resource.Error(errorMessage, data))
                }
            }
        }
    }

    //Only emit new value
    @MainThread
    private fun setValue(value: Resource<ResultType>) {
        if (result.value != value) {
            result.value = value
        }
    }

    fun asLiveData(): LiveData<Resource<ResultType>> = result

    //Called to save the result
    @WorkerThread
    protected abstract fun saveCallResult(item: RequestType)

    //Called with the data in the db to decide to weather to fetch
    //potentially updated data from the network
    @MainThread
    protected abstract fun shouldFetch(data: ResultType?): Boolean

    //Called to get the cached data from db
    @MainThread
    protected abstract fun loadFromDb(): LiveData<ResultType>

    //Called to create API call
    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestType>>

    //Called when the fetch fails. The child class may want to reset components
    //like rate limiter
    protected open fun onFetchFailed() {}
}